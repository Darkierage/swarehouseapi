#!/bin/bash

SERVICE_FILE_NAME="swarehouse__api"
DATABASE_NAME="swarehouse_dev"

cd ..
pwd
sudo -u postgres psql -f ./init/swarehouse_init_db.sql
apt-get install python3-venv && python3 -m venv venv
source ./venv/bin/activate
pip install -r requirements.txt
python manage.py makemigrations && python manage.py migrate
python manage.py createsuperuser
sudo -u postgres psql -d ${DATABASE_NAME} -f ./init/swarehouse_init_data.sql
python manage.py collectstatic
cp ./init/${SERVICE_FILE_NAME}.service /etc/systemd/system/
systemctl start ${SERVICE_FILE_NAME}
systemctl enable ${SERVICE_FILE_NAME}
systemctl daemon-reload
