INSERT INTO "warehouseApi_warehouse" (name, owner_id, icon) VALUES ('Grota', 1, 'spa');
INSERT INTO "warehouseApi_warehouse" (name, owner_id, icon) VALUES ('Ludowa', 1, 'bubble_chart');

INSERT INTO auth_group (name) VALUES ('Grota');
INSERT INTO auth_group (name) VALUES ('Ludowa');

INSERT INTO guardian_groupobjectpermission (object_pk, content_type_id, group_id, permission_id)
VALUES (
(SELECT id from "warehouseApi_warehouse" WHERE name = 'Grota'),
(SELECT id FROM django_content_type WHERE model = 'warehouse'),
(SELECT id FROM auth_group WHERE name = 'Grota'),
(SELECT id FROM auth_permission WHERE codename = 'can_edit' AND content_type_id = (SELECT id FROM django_content_type WHERE model = 'warehouse')));

INSERT INTO guardian_groupobjectpermission (object_pk, content_type_id, group_id, permission_id)
VALUES (
(SELECT id from "warehouseApi_warehouse" WHERE name = 'Ludowa'),
(SELECT id FROM django_content_type WHERE model = 'warehouse'),
(SELECT id FROM auth_group WHERE name = 'Ludowa'),
(SELECT id FROM auth_permission WHERE codename = 'can_edit' AND content_type_id = (SELECT id FROM django_content_type WHERE model = 'warehouse')));

INSERT INTO "warehouseApi_contractor" (name, "isEmployee", owner_id, "isSystem", created, modified) VALUES ('!Anonimowy', false, 2, true, now(), now());
