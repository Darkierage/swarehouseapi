CREATE DATABASE <databaseName>;
CREATE USER <databaseUserName> WITH PASSWORD '<databaseUserPassword>';
ALTER ROLE <databaseUserName> SET client_encoding TO 'utf8';
ALTER ROLE <databaseUserName> SET default_transaction_isolation TO 'read committed';
ALTER ROLE <databaseUserName> SET timezone TO 'UTC';
GRANT ALL PRIVILEGES ON DATABASE <databaseName> TO <databaseUserName>;
