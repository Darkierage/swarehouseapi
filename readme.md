## Requirements

- root access on server
- configured git with ssh
- configured gitlab-runner with shell executor

## Setup

**Use root privileges for entire setup**

### `SERVICE`

1. Clone project using git to directory: */data/sw/urlPrefix* (urlPrefix is domain like directory):
```console
git clone git@gitlab.com:Darkierage/swarehouseapi.git
```
2. Create directory *init* inside root project.
3. Copy all files from *init_template* directory to *init*.
4. Edit *config.json* from *init*.
5. Edit *swarehouse_init.db.sql* from *init* using configuration used in step 3.
6. Rename and edit *swarehouse_template_api.service* from *init*.
7. Edit *swarehouse_init.sh* from *init* setting *SERVICE_FILE_NAME* with name of file from step 5.
8. Run *swarehouse_init.sh* script.
9. Add lines to /etc/sudoers for *start/stop/restart* systemctl commends:
```
%gitlab-runner ALL=(ALL) NOPASSWD: /bin/systemctl start swarehouse_<template>_api.service
%gitlab-runner ALL=(ALL) NOPASSWD: /bin/systemctl stop swarehouse_<template>_api.service
%gitlab-runner ALL=(ALL) NOPASSWD: /bin/systemctl restart swarehouse_<template>_api.service
```
10. Run command: 
```console
chmod -R 777 swarehouseapi
```
### `NGINX`

1. Add *proxy_pass* for api like (*port* and *urlPrefix* defined in *swarehouse_template_api.service* and *config.json* files in previous steps):
```
location /sw/<urlPrefix>/api {
        proxy_pass http://localhost:<port>;
}
```
2. Add *proxy_pass* for admin like (*port* and *urlPrefix* defined in *swarehouse_template_api.service* and *config.json* files in previous steps):
```
location /sw/<urlPrefix>/admin {
        proxy_pass http://localhost:<port>;
}
```

3. Add *alias* for static files needed for admin panel
```
location /sw/<urlPrefix>/static {
        alias /data/sw/<urlPrefix>/swarehouseapi/static;
}
```
