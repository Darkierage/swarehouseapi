from django.db import models


class Contractor(models.Model):
    name = models.CharField(max_length=30)
    isEmployee = models.BooleanField(default=False)
    isSystem = models.BooleanField(default=False)
    comments = models.CharField(max_length=255, blank=True, null=True)

    created = models.DateTimeField(auto_now_add=True, blank=True)
    modified = models.DateTimeField(auto_now=True, blank=True)
    owner = models.ForeignKey('auth.User', related_name='contractor_owner', on_delete=models.PROTECT, default=1)

    def __str__(self):
        return self.name


class Warehouse(models.Model):
    class Meta:
        permissions = (
            ("can_edit", "Can edit"),
        )

    name = models.CharField(max_length=255)
    owner = models.ForeignKey('auth.User', related_name='warehouse_owner', on_delete=models.PROTECT, default=1)
    icon = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class Category(models.Model):
    class Meta:
        verbose_name_plural = "categories"

    name = models.CharField(max_length=30)
    warehouse = models.ForeignKey('warehouse', related_name='category',
                                  on_delete=models.PROTECT, null=False)
    icon = models.CharField(max_length=255)
    created = models.DateTimeField(auto_now_add=True, blank=True)
    modified = models.DateTimeField(auto_now=True, blank=True)
    owner = models.ForeignKey('auth.User', related_name='category_owner', on_delete=models.PROTECT, default=1)

    def __str__(self):
        return self.name


class Item(models.Model):
    name = models.CharField(max_length=30)
    amount = models.DecimalField(max_digits=20, decimal_places=5)
    netPrice = models.DecimalField(max_digits=11, decimal_places=2)
    tax = models.DecimalField(max_digits=7, decimal_places=2)
    margin = models.DecimalField(max_digits=7, decimal_places=2)
    comments = models.CharField(max_length=255, blank=True, null=True)
    warehouse = models.ForeignKey('warehouse', on_delete=models.PROTECT, null=False, related_name='warehouse')
    category = models.ForeignKey('category', on_delete=models.PROTECT, null=False, related_name='category')
    customMargin = models.BooleanField(default=False)

    owner = models.ForeignKey('auth.User', related_name='item_owner', on_delete=models.PROTECT, default=1)
    created = models.DateTimeField(auto_now_add=True, blank=True)
    modified = models.DateTimeField(auto_now=True, blank=True)

    def __str__(self):
        return self.name


class ItemHistory(models.Model):
    item = models.ForeignKey('item', on_delete=models.CASCADE, null=False)
    name = models.CharField(max_length=30)
    amount = models.DecimalField(max_digits=20, decimal_places=5)
    netPrice = models.DecimalField(max_digits=11, decimal_places=2)
    tax = models.DecimalField(max_digits=7, decimal_places=2)
    margin = models.DecimalField(max_digits=7, decimal_places=2)
    comments = models.CharField(max_length=255, blank=True, null=True)
    historyComments = models.CharField(max_length=255, blank=True, null=True)
    isDelivery = models.BooleanField(default=False)
    contractor = models.ForeignKey('contractor', on_delete=models.CASCADE, null=False)
    customMargin = models.BooleanField(default=False)

    owner = models.ForeignKey('auth.User', related_name='item_history_owner', on_delete=models.PROTECT, default=1)
    created = models.DateTimeField(auto_now_add=True, blank=True)
    modified = models.DateTimeField(auto_now=True, blank=True)

    def __str__(self):
        return self.name


class ItemFaq(models.Model):
    name = models.CharField(max_length=30)
    content = models.TextField(blank=True, null=True)

    owner = models.ForeignKey('auth.User', related_name='item_faq_owner', on_delete=models.PROTECT, default=1)
    created = models.DateTimeField(auto_now_add=True, blank=True)
    modified = models.DateTimeField(auto_now=True, blank=True)

    def __str__(self):
        return self.name
