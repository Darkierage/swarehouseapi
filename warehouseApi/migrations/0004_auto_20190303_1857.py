# Generated by Django 2.1.7 on 2019-03-03 17:57

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('warehouseApi', '0003_auto_20190303_1846'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='category',
            options={'verbose_name_plural': 'categories'},
        ),
        migrations.AlterField(
            model_name='category',
            name='owner',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, related_name='category_owner', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='category',
            name='warehouse_id',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='category_warehouse_id', to='warehouseApi.Warehouse'),
        ),
    ]
