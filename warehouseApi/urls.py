from django.urls import path

from warehouseApi.views import ItemV, ItemHistory, get_warehouse_crud, ContractorV, get_warehouse_balance, \
    get_special_crud
from .views import login, WarehouseV, CategoryV, ItemFaqV

urlpatterns = [
    path('api/login', login),
    path('api/crud', get_warehouse_crud),
    path('api/balance', get_warehouse_balance),
    path('api/warehouses', WarehouseV.as_view()),
    path('api/categories', CategoryV.as_view()),
    path('api/category/<int:pk>', CategoryV.as_view()),
    path('api/items', ItemV.as_view()),
    path('api/item/<int:pk>', ItemV.as_view()),
    path('api/histories', ItemHistory.as_view()),
    path('api/contractors', ContractorV.as_view()),
    path('api/contractor/<int:pk>', ContractorV.as_view()),
    path('api/faq', ItemFaqV.as_view()),
    path('api/faq/<int:pk>', ItemFaqV.as_view()),
    path('api/permission', get_special_crud),
]
