from rest_framework import permissions
from warehouseApi.models import Warehouse


class IsItemAllowed(permissions.BasePermission):

    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return True

        if request.method == 'POST' or request.method == 'PUT' or request.method == 'DELETE':
            warehouse = Warehouse.objects.get(id=obj.warehouse.id)
            return request.user.has_perm('can_edit', warehouse)

        return False

    def has_permission(self, request, view):
        if request.method in permissions.SAFE_METHODS:
            return True

        if request.method == 'POST':
            warehouse = Warehouse.objects.get(id=request.data['warehouse'])
            return request.user.has_perm('can_edit', warehouse)

        if request.method == 'PUT':
            warehouse = Warehouse.objects.get(id=request.data['warehouse'])
            return request.user.has_perm('can_edit', warehouse)

        if request.method == 'DELETE':
            return True

        return False


class IsItemFaqAllowed(permissions.BasePermission):

    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return True

        if request.method in permissions.SAFE_METHODS:
            return True

        if request.method == 'POST':
            return request.user.has_perm('warehouseApi.change_itemfaq')

        if request.method == 'PUT':
            return request.user.has_perm('warehouseApi.add_itemfaq')

        if request.method == 'DELETE':
            return request.user.has_perm('warehouseApi.delete_itemfaq')

        return False

    def has_permission(self, request, view):
        if request.method in permissions.SAFE_METHODS:
            return True

        if request.method == 'POST':
            return request.user.has_perm('warehouseApi.change_itemfaq')

        if request.method == 'PUT':
            return request.user.has_perm('warehouseApi.add_itemfaq')

        if request.method == 'DELETE':
            return True

        return False
