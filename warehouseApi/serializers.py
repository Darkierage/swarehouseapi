from datetime import datetime

from rest_framework.fields import ReadOnlyField
from rest_framework.relations import PrimaryKeyRelatedField
from tzlocal import get_localzone

from rest_framework import serializers
from warehouseApi.models import Warehouse, Contractor, Item, ItemHistory, ItemFaq
from warehouseApi.models import Category


class ContractorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Contractor
        fields = ('id', 'name', 'isEmployee', 'owner', 'modified', 'isSystem', 'comments')

    def create(self, validated_data):
        return Contractor.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.name = validated_data.get('name', instance.name)
        instance.isEmployee = validated_data.get('isEmployee', instance.isEmployee)
        instance.comments = validated_data.get('comments', instance.comments)
        instance.save()

        return instance


class WarehouseSerializer(serializers.Serializer):
    def update(self, instance, validated_data):
        pass

    id = serializers.IntegerField(read_only=True)
    name = serializers.CharField(required=False, allow_blank=True, max_length=255)
    icon = serializers.CharField(max_length=255)

    def create(self, validated_data):
        return Warehouse.objects.create(**validated_data)


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ('id', 'name', 'warehouse', 'icon', 'owner')

    def update(self, instance, validated_data):
        instance.name = validated_data.get('name', instance.name)
        instance.save()

        return instance


class ItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = Item
        fields = ('id', 'name', 'amount', 'netPrice', 'tax', 'margin', 'comments', 'warehouse', 'category',
                  'customMargin', 'owner', 'modified')

    def update(self, instance, validated_data):
        request = self.context['request']

        if validated_data.get('amount'):
            ih = ItemHistory()
            ih.item = instance
            ih.name = instance.name
            ih.amount = request.data['editAmount']
            ih.netPrice = instance.netPrice
            ih.tax = instance.tax
            ih.margin = instance.margin
            ih.comments = instance.comments
            ih.historyComments = request.data['historyComments']
            ih.isDelivery = request.data['isDelivery']
            ih.contractor = Contractor.objects.get(id=request.data['contractor'])
            ih.customMargin = instance.customMargin
            ih.owner = request.user
            ih.save()

        instance.name = validated_data.get('name', instance.name)
        instance.amount = validated_data.get('amount', instance.amount)
        instance.netPrice = validated_data.get('netPrice', instance.netPrice)
        instance.tax = validated_data.get('tax', instance.tax)
        instance.margin = validated_data.get('margin', instance.margin)
        instance.comments = validated_data.get('comments', instance.comments)
        instance.customMargin = validated_data.get('customMargin', instance.customMargin)

        instance.save()

        return instance


class ItemFaqSerializer(serializers.ModelSerializer):
    class Meta:
        model = ItemFaq
        fields = ('id', 'name', 'content', 'owner', 'modified')

    def update(self, instance, validated_data):
        instance.name = validated_data.get('name', instance.name)
        instance.content = validated_data.get('content', instance.content)
        instance.save()

        return instance


class ItemHistorySerializer(serializers.ModelSerializer):
    warehouse_id = ReadOnlyField(source='item.warehouse.id')
    category_id = ReadOnlyField(source='item.category.id')

    class Meta:
        model = ItemHistory
        fields = (
            'id', 'item', 'warehouse_id', 'category_id', 'name', 'amount', 'netPrice', 'tax', 'margin', 'comments',
            'historyComments',
            'isDelivery', 'created', 'contractor',
            'owner', 'modified')
