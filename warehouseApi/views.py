from django.utils import dateparse

from django.contrib.auth import authenticate
from django.views.decorators.csrf import csrf_exempt
from rest_framework import mixins, generics
from rest_framework.authtoken.models import Token
from rest_framework.decorators import api_view, permission_classes
from rest_framework.filters import SearchFilter
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.status import (
    HTTP_400_BAD_REQUEST,
    HTTP_404_NOT_FOUND,
    HTTP_200_OK,
    HTTP_409_CONFLICT
)
from rest_framework.response import Response
from django_filters.rest_framework import DjangoFilterBackend

from warehouseApi.filters import ItemHistoryFilter, ItemFilter
from warehouseApi.models import Warehouse, Category, Contractor, Item, ItemHistory, ItemFaq
from warehouseApi.permissions import IsItemAllowed, IsItemFaqAllowed
from warehouseApi.serializers import WarehouseSerializer, CategorySerializer, ContractorSerializer, ItemSerializer, \
    ItemHistorySerializer, ItemFaqSerializer
from django.db.models.functions import Lower
from django.db.models import Sum, F


class ContractorV(generics.ListCreateAPIView,
                  generics.RetrieveUpdateDestroyAPIView):
    queryset = Contractor.objects.all().order_by('-isSystem', '-isEmployee', Lower('name'))
    serializer_class = ContractorSerializer
    filter_backends = (DjangoFilterBackend, SearchFilter,)
    filterset_fields = ()
    search_fields = ('name',)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

    def get(self, request, *args, **kwargs):
        if kwargs:
            return self.retrieve(request, *args, **kwargs)
        else:
            return self.list(request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        contractor = Contractor.objects.get(id=kwargs['pk'])

        modified = dateparse.parse_datetime(request.data['modified'])
        if contractor.modified != modified:
            return Response(None, status=HTTP_409_CONFLICT)

        if contractor.isSystem:
            return Response(None, state=HTTP_400_BAD_REQUEST)

        return self.partial_update(request, *args, **kwargs)


class WarehouseV(mixins.ListModelMixin,
                 mixins.CreateModelMixin,
                 generics.GenericAPIView):
    queryset = Warehouse.objects.all()
    serializer_class = WarehouseSerializer
    pagination_class = None

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class CategoryV(mixins.ListModelMixin,
                mixins.RetrieveModelMixin,
                mixins.CreateModelMixin,
                mixins.UpdateModelMixin,
                mixins.DestroyModelMixin,
                generics.GenericAPIView):
    queryset = Category.objects.all().order_by(Lower('name'))
    serializer_class = CategorySerializer
    filter_backends = (DjangoFilterBackend, SearchFilter,)
    filterset_fields = ('warehouse',)
    search_fields = ('name',)
    pagination_class = None

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

    def get(self, request, *args, **kwargs):
        if kwargs:
            return self.retrieve(request, *args, **kwargs)
        else:
            return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)


class ItemV(mixins.ListModelMixin,
            mixins.RetrieveModelMixin,
            mixins.CreateModelMixin,
            mixins.UpdateModelMixin,
            mixins.DestroyModelMixin,
            generics.GenericAPIView):
    queryset = Item.objects.all().order_by(Lower('name'))
    serializer_class = ItemSerializer
    permission_classes = (IsAuthenticated, IsItemAllowed)
    filter_backends = (DjangoFilterBackend, SearchFilter,)
    filterset_class = ItemFilter
    search_fields = ('name', 'comments', 'amount', 'netPrice',)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

    def put(self, request, *args, **kwargs):
        item = Item.objects.get(id=kwargs['pk'])

        modified = dateparse.parse_datetime(request.data['modified'])
        if item.modified != modified:
            return Response(None, status=HTTP_409_CONFLICT)

        return self.partial_update(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        if kwargs:
            return self.retrieve(request, *args, **kwargs)
        else:
            return self.list(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)


class ItemFaqV(mixins.ListModelMixin,
               mixins.RetrieveModelMixin,
               mixins.CreateModelMixin,
               mixins.UpdateModelMixin,
               mixins.DestroyModelMixin,
               generics.GenericAPIView):
    queryset = ItemFaq.objects.all().order_by(Lower('name'))
    serializer_class = ItemFaqSerializer
    permission_classes = (IsAuthenticated, IsItemFaqAllowed)
    filter_backends = (DjangoFilterBackend, SearchFilter,)
    search_fields = ('name', 'content')

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

    def put(self, request, *args, **kwargs):
        item_faq = ItemFaq.objects.get(id=kwargs['pk'])

        modified = dateparse.parse_datetime(request.data['modified'])
        if item_faq.modified != modified:
            return Response(None, status=HTTP_409_CONFLICT)

        return self.partial_update(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        if kwargs:
            return self.retrieve(request, *args, **kwargs)
        else:
            return self.list(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)


class ItemHistory(generics.ListAPIView):
    queryset = ItemHistory.objects.all().order_by('-created')
    serializer_class = ItemHistorySerializer
    filter_backends = (DjangoFilterBackend, SearchFilter,)
    filterset_class = ItemHistoryFilter
    search_fields = ('name', 'comments', 'amount', 'netPrice', 'historyComments', 'contractor__name',)


@csrf_exempt
@api_view(["POST"])
@permission_classes((AllowAny,))
def login(request):
    username = request.data.get("username")
    password = request.data.get("password")
    if username is None or password is None:
        return Response({'error': 'Please provide both username and password'},
                        status=HTTP_400_BAD_REQUEST)
    user = authenticate(username=username, password=password)
    if not user:
        return Response({'error': 'Invalid Credentials'},
                        status=HTTP_404_NOT_FOUND)
    token, _ = Token.objects.get_or_create(user=user)
    return Response({'token': token.key},
                    status=HTTP_200_OK)


@api_view(['GET'])
def get_warehouse_balance(request):
    balance = {'totalBalance': Item.objects.all().aggregate(total=Sum(
        F('netPrice') * F('amount')))['total']}

    for warehouse in Warehouse.objects.all():
        warehouse_id = warehouse.id
        balance[warehouse_id] = Item.objects.all().filter(warehouse=warehouse_id).aggregate(total=Sum(
            F('netPrice') * F('amount')))['total']

    return Response(balance, status=HTTP_200_OK)


@api_view(['GET'])
def get_warehouse_crud(request):
    warehouses = Warehouse.objects.all()

    crud = {}

    for warehouse in warehouses:
        crud[warehouse.id] = request.user.has_perm('can_edit', warehouse)

    return Response(crud, status=HTTP_200_OK)


@api_view(['GET'])
def get_special_crud(request):
    crud = {
        'faq': {
            'create': request.user.has_perm('warehouseApi.add_itemfaq'),
            'read': True,
            'update': request.user.has_perm('warehouseApi.change_itemfaq'),
            'delete': request.user.has_perm('warehouseApi.delete_itemfaq'),
        }
    }

    return Response(crud, status=HTTP_200_OK)

# @api_view(["POST"])
# def set_permission_for_group(request):
#     group = Group.objects.get(id=request.POST['group_id'])
#     # categories = Category.objects.filter(warehouse_id=request.POST['warehouse_id'])
#
#     warehouse = Warehouse.objects.filter(id=request.POST['warehouse_id'])
#     assign_perm('can_edit', group, warehouse)
#
#     # for category in categories:
#     #     assign_perm('edit_category', group, category)
#
#     return Response(status=HTTP_200_OK)
