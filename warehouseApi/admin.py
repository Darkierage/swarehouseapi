from django.contrib import admin

from warehouseApi.models import Warehouse, Category, Contractor, Item, ItemHistory, ItemFaq

admin.site.register(Contractor)
admin.site.register(Warehouse)
admin.site.register(Category)
admin.site.register(Item)
admin.site.register(ItemHistory)
admin.site.register(ItemFaq)
