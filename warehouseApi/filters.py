from django_filters import rest_framework as filters

from warehouseApi.models import ItemHistory, Item


class ItemHistoryFilter(filters.FilterSet):
    created = filters.DateFilter(input_formats=['%Y-%m-%d', '%d-%m-%Y'], lookup_expr='contains')

    class Meta:
        model = ItemHistory
        fields = ['created', 'item', 'contractor', 'isDelivery']


class ItemFilter(filters.FilterSet):
    lastUpdateDate = filters.DateFilter(input_formats=['%Y-%m-%d', '%d-%m-%Y'], lookup_expr='contains')

    class Meta:
        model = Item
        fields = ['warehouse', 'category', 'lastUpdateDate']
